/**
 * Created by lichong on 3/9/2018.
 */

import React from 'react';
import { Badge, Button, Dropdown, Menu , Spin } from 'antd';

import {Link} from 'react-router-dom'

import './minicart.css'

class Item extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    removeCartItem () {
        this.props.onRemoveItem(this.props.data);
    }

    render() {
        return (
            <div className="minicart-item">
                <img className="pull-left" src={this.props.data.prodImg}/>
                <span className="pull-left prod-name" >{this.props.data.prodName}</span>
                <div className="pull-right">
                    <span className="prod-price">{this.props.data.price} x{this.props.data.quantity}</span><br/>
                    <a href="javascript:void(0)" onClick={(e) => this.removeCartItem(e)}>删除</a>
                </div>
            </div>
        );
    }
}

export default Item;