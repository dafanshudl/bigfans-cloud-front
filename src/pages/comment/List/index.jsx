/**
 * Created by lichong on 2/23/2018.
 */

import React, {Component} from 'react';
import {Row, Col, List, Radio, Avatar, Button , Rate , Icon} from 'antd';

import HttpUtils from 'utils/HttpUtils'

const data = [
    {
        title: 'Ant Design Title 1',
    },
    {
        title: 'Ant Design Title 2',
    },
    {
        title: 'Ant Design Title 3',
    },
    {
        title: 'Ant Design Title 4',
    },
];

class CommentList extends React.Component {

    state = {
        comments : [],
        score : {}
    }

    componentDidMount(){
        let pgId = this.props.product.pgId;
        let self = this;
        HttpUtils.listComments(pgId , {
            success (resp){
                self.setState({comments : resp.data.commentPage.data , score : resp.data.score})
            },
            error (){

            }
        })
    }


    render() {
        return (
            <div>
                <List
                    itemLayout="horizontal"
                    dataSource={this.state.comments}
                    renderItem={item => (
                        <List.Item actions={[<Icon type="like-o" />]}>
                            <List.Item.Meta
                                avatar={<Avatar
                                    src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>}
                                title={<Rate value={item.rate} disabled/>}
                                description={item.content}
                            />
                        </List.Item>
                    )}
                />
            </div>
        );
    }
}
export default CommentList;