/**
 * Created by lichong on 2/22/2018.
 */

import React from 'react';

import HttpUtils from '../../../utils/HttpUtils';

class ProductDesc extends React.Component {
    state = {
        description : '',
    };

    componentDidMount () {
        var self = this;
        HttpUtils.getProductDesc({params : {prodId : this.props.prodId}} , {
            success : function (response) {
                self.setState({description : response.data})
            }
        })
    }

    render() {
        return (
            <div>
                {this.state.description}
            </div>
        );
    }
}
export default ProductDesc;